/*
 * File:   main.c
 * Author: Brandy
 * Reference: https://electrosome.com/pwm-pic-microcontroller-mplab-xc8/
 * Created on August 31, 2016, 9:22 PM
 */


#include <xc.h>
#include "Alteri.h"
/*
void main(void) {
    TRISCbits.RC0=0;
    PR2=0x7C;
    TMR2=0;
    T2CON=0x05;
    CCP1CONbits.CCP1M=0xC;//PWM Mode
    CCP1CONbits.DC1B=0;//These bits are the two LSbs (bit 1 and bit 0) of the 10-bit PWM duty cycle.
    //The eight MSbs of the dutycycle are found in CCPR1L.
    CCPR1L=0x1A;
    
    while(1){
        
    }
    return;
}
*/
#define TMR2PRESCALE 16
 
long freq;
int CCP1X;
int CCP1Y;
int CCP2X;
int CCP2Y;

int PWM_Max_Duty()
{
  int stuff=0;
  stuff=(_XTAL_FREQ/(freq*TMR2PRESCALE));
  return stuff;
}

PWM1_Init(long fre)
{
  PR2 = ((int)(_XTAL_FREQ/(fre*4*TMR2PRESCALE)) - 1);
  freq = fre;
}

PWM2_Init(long fre)
{
  PR2 = ((int)(_XTAL_FREQ/(fre*4*TMR2PRESCALE)) - 1);
  freq = fre;
}

PWM1_Duty(unsigned int duty)
{
  int Max_Duty=0;
  long d=0;
  Max_Duty=PWM_Max_Duty();
  if(duty<1024)
  {
    d=duty*Max_Duty;
    duty = ((int)(d/1023));
    CCP1X = duty & 2;
    CCP1Y = duty & 1;
    CCPR1L = ((int)duty)>>2;
  }
}

PWM2_Duty(unsigned int duty)
{
  int Max_Duty=0;
  long d=0;
  Max_Duty=PWM_Max_Duty();
  if(duty<1024)
  {
    d=duty*Max_Duty;
    duty = ((int)(d/1023));
    CCP2X = duty & 2;
    CCP2Y = duty & 1;
    CCPR2L = ((int)duty)>>2;
  }
    /*
  if(duty<1024)
  {
    duty = ((int)((float)duty/1023)*PWM_Max_Duty());
    CCP2X = duty & 2;
    CCP2Y = duty & 1;
    CCPR2L = ((int)duty)>>2;
  }
     * */
}

PWM1_Start()
{
  CCP1M3 = 1;
  CCP1M2 = 1;
  #if TMR2PRESCALAR == 1
    T2CKPS0 = 0;
    T2CKPS1 = 0;
  #elif TMR2PRESCALAR == 4
    T2CKPS0 = 1;
    T2CKPS1 = 0;
  #elif TMR2PRESCALAR == 16
    T2CKPS0 = 1;
    T2CKPS1 = 1;
  #endif
  TMR2ON = 1;
  TRISC2 = 0;
}

PWM1_Stop()
{
  CCP1M3 = 0;
  CCP1M2 = 0;
}

PWM2_Start()
{
  CCP2M3 = 1;
  CCP2M2 = 1;
  #if TMR2PRESCALE == 1
    T2CKPS0 = 0;
    T2CKPS1 = 0;
  #elif TMR2PRESCALE == 4
    T2CKPS0 = 1;
    T2CKPS1 = 0;
  #elif TMR2PRESCALE == 16
    T2CKPS0 = 1;
    T2CKPS1 = 1;
  #endif
    TMR2ON = 1;
    TRISC1 = 0;
}

PWM2_Stop()
{
  CCP2M3 = 0;
  CCP2M2 = 0;
}

void main()
{
  unsigned int i=0,j=0;
  PWM1_Init(5000);
  PWM2_Init(5000);
  TRISD = 0xFF;
  TRISB = 0;
  PWM1_Duty(255);
  PWM2_Duty(255);
  PWM1_Start();
  PWM2_Start();
  do
  {
    if(RD0 == 0 && i<1000)
      i=i+10;
    if(RD1 == 0 && i>0)
      i=i-10;
    if(RD2 == 0 && j<1000)
      j=j+10;
    if(RD3 == 0 && j>0)
      j=j-10;
    PWM1_Duty(i);
    PWM2_Duty(j);
    //delay_ms(10);
  }
  while(1);
}
