/*
 * File:   maino.c
 * Author: Brandy
 * Reference: http://www.micro-examples.com/public/microex-navig/doc/097-pwm-calculator.html
 *            Used for code Generator
 *            http://www.microchip.com/forums/m705595.aspx
 *            Used for the formulas of PWM
 * Created on September 7, 2016, 1:59 AM
 */


#include <xc.h>
#include "Alteri.h"
// Tosc = 1/48 Mhz = 0.000000020833 
// PWM Period = 4 x 0.000000020833 x     256     x        16            = 0.000341333 
// PWM Period = 4 x     TOSC       x [(PR2) + 1] x (TMR2 Prescale Value)
// PWM Frequency = 1/PWM Period = 1/0.000341333 = 2.92969 kHz 
// PWM Duty Cycle = (CCPRXL:CCPXCON<5:4>) x      TOSC      x (TMR2 Prescale Value)
// PWM Duty Cycle = (CCPR1L:CCP1CON<5:4>) x 0.000000020833 X         16

void main() {
    unsigned char dc;
    TRISC = 0; // set PORTC as output
    PORTC = 0; // clear PORTC
    TRISD = 0;
    PORTD = 0;
    /*
     * configure CCP module as 2929.69 Hz PWM output
     */
    PR2 = 0b11111111;
    //pg. 150 Period Register This is used to seta frequency
    T2CON = 0b00000111;
    //pg. 139 Timer2 is on and Prescaler is 16
    CCP1CON = 0b00111100;
    //pg. 145 DCxB1:DCxB0These bits are the two LSbs (bit 1 and bit 0) 
    //of the 10-bit PWM duty cycle. The eight MSbs of the duty
    //cycle are found in CCPR1L. Both are set as 1
    //CCPxM3:CCPxM0: CCPx Module Mode Select bits
    //11xx = PWM mode
    CCP2CON = 0b00111100;
    while (1) {
        /*
         * PWM resolution is 10 bits
         * don't use last 2 less significant bits CCPxCON,
         * so only CCPRxL have to be touched to change duty cycle
         */
        for (dc = 0; dc < 128; dc++) {
            LATD = dc;
            CCPR1L = dc;//RC2
            //Defines de DutyCycle
            CCPR2L = 128 - dc;//RC1
            delay_ms(100);
        }
        for (dc = 127; dc > 0; dc--) {
            LATD = dc;
            CCPR1L = dc;//
            CCPR2L = 128 - dc;
            delay_ms(100);
        }
    }
}